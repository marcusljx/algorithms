// Package josephus presents brute-force and optimal functions for calculating the resultant position
// in the Josephus Problem.
//
// The Josephus Problem
//
// The Josephus Problem is formulated as such:
//
//   Given n people in a circle, starting from the first person,
//   each person removes/murders the next available person in order from the circle,
//   until only 1 person is left remaining.
//
//   An example of this is such that there are 7 people (indices 0-6),
//    - person 0 removes person 1
//    - person 2 removes person 3
//    - person 4 removes person 5
//    - person 6 removes person 0
//    - person 2 removes person 4
//    - person 6 removes person 2
//   Hence person 6 is the last person standing.
//
//   Design a function f such that for all n >= 1,
//   f(n) returns the position of the person who will remain after all others are removed.
package josephus

import (
	"math/bits"
)

// BruteForce finds the Josephus position of n people in
// O(n log n) time and
// O(n) space
func BruteForce(n int) int {
	if n <= 1 {
		return 0
	}
	var (
		arr       = make([]int, n)
		remaining = n
		curr      = 0
	)

	for remaining > 1 {
		// find next killer
		for ; arr[curr%n] != 0; curr++ {
		}

		// find next victim
		victimPos := curr + 1
		for ; arr[victimPos%n] != 0; victimPos++ {
		}

		//fmt.Printf("[%d] kills [%d]  :: ", curr%n, victimPos%n)

		arr[victimPos%n] = -1
		remaining--
		curr = (victimPos + 1) % n
		//fmt.Printf("curr == %d\n", curr)
	}

	for ; arr[curr%n] != 0; curr++ {
	}

	return curr % n
}

// Optimal finds the Josephus position of n people in
// O(1) time and
// O(1) space.
func Optimal(n int) int {
	if n <= 1 {
		return 0
	}

	// This section is broken into particular segments simply to show the steps.
	// However, it can actually be represented as a single-line return
	var (
		bitWidthMinus1           = bits.Len(uint(n)) - 1
		largestPowerOf2LessThanN = 1 << bitWidthMinus1
		offset                   = n - largestPowerOf2LessThanN
	)

	return offset * 2

	// One-Line answer
	//return 2 * (n - (1 << (bits.Len(uint(n)) - 1)))
}
