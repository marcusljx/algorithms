package josephus

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBruteForce(t *testing.T) {
	check := func(n, expected int) {
		t.Run(fmt.Sprint(n), func(t2 *testing.T) {
			actual := BruteForce(n)
			if !cmp.Equal(actual, expected) {
				t2.Error(cmp.Diff(actual, expected))
			}
		})
	}

	check(1, 0)
	check(2, 0)
	check(3, 2)
	check(4, 0)
	check(5, 2)
	check(6, 4)
	check(7, 6)
	check(8, 0)
	check(9, 2)
	check(10, 4)
	check(11, 6)
	check(12, 8)
	check(13, 10)
	check(14, 12)
	check(15, 14)
	check(16, 0)
	check(17, 2)
	check(18, 4)
	check(19, 6)
	check(20, 8)
	check(21, 10)
	check(22, 12)
}

func TestOptimal(t *testing.T) {
	for i := 0; i < 100; i++ {
		t.Run(fmt.Sprint(i), func(t2 *testing.T) {
			actual, expected := Optimal(i), BruteForce(i)
			if !cmp.Equal(actual, expected) {
				t2.Errorf("Optimal => %d, BruteForce => %d", actual, expected)
			}
		})
	}
}
func BenchmarkOptimal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Optimal(i)
	}
}
